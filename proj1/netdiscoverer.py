#Net Discoverer
# Sean Higley & Nathan Chanachai
#IMPORTANT: if on linux distribution run with sudo and have scapy/netifaces installed on your machine


from scapy import all as scapy
import netifaces
from ipaddress import IPv4Address, IPv4Network

#HELPER FUNCITONS AND LISTS
classA = IPv4Network("10.0.0.0/8")  # or IPv4Network("10.0.0.0/8")
classB = IPv4Network("172.16.0.0/12")  # or IPv4Network("172.16.0.0/12")
classC = IPv4Network("192.168.0.0/16")  # or IPv4Network("192.168.0.0/16")

def get_nmask_inf(ipadd):
    if IPv4Address(ipadd) in classA:
        return str(ipadd)+"/8"
    if IPv4Address(ipadd) in classB:
        return str(ipadd)+"/16"
    if IPv4Address(ipadd) in classC:
        return str(ipadd)+"/24"
    else:
        return str(ipadd)+"/0"

#END HELPERS


scapy.conf.verbose = 0 #reduces verbosity of scapy
interface_names = scapy.get_if_list()



#print interfaces:
print("Interfaces:")
for iface in netifaces.interfaces():
    if iface in interface_names:
        iface_details = netifaces.ifaddresses(iface)
        if netifaces.AF_INET in iface_details:
            print("   "+iface)

print("-----")

#print interface details
interfaces_tuple_holder = [] #(NAME, IP, MAC)
print("Interface details:")
for iface in netifaces.interfaces():
    if iface in interface_names:
        iface_details = netifaces.ifaddresses(iface)
        
        if netifaces.AF_INET in iface_details:
            temp_tuple = (iface, get_nmask_inf(iface_details[netifaces.AF_INET][0]['addr']), str(scapy.get_if_hwaddr(iface)),iface_details[netifaces.AF_INET][0]['addr'] +'/24' )
            interfaces_tuple_holder.append(temp_tuple)
            print("   "+ str(temp_tuple[0]))
            print("      IP: "+temp_tuple[1])
            print("      MAC: "+temp_tuple[2])
print("-----")

# scan network

for ip_i in interfaces_tuple_holder:
    if 'lo' in ip_i[0]:
        continue
    
    target_IP_range = ip_i[3]
    pkt = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")/scapy.ARP(pdst=target_IP_range) 
    print("Scanning interface: "+ip_i[0])
    print("Available: ")
    av = [] #available interfaces holder
    result = scapy.srp(pkt, timeout=3, verbose=0)[0]
    for s, r in result:
        av.append({'ip': r.psrc, 'mac': r.hwsrc})
    
    print("IP" + " "*17+"MAC")
    for cli in av:
        print("{:16}    {}".format(cli['ip'], cli['mac']))

    print()
