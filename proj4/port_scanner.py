# Proj 4 Port Scanner
# Sean Higley & Nathan Chanachai

from scapy.all import *

import re
import sys

def udp_response(resp, pnum): # Helper function for parsing the UDP response
    status_str = ['Status: Open         ', 'Status: Open|Filtered       ', 'Status: Closed       ', 'Status: Filtered       ']
    p_str = 'Port: [' + str(pnum) + ']        '
    if resp != None:
        if resp.type == 3:
            if resp.code in [1,2,9,10,13]:
                print(p_str + status_str[3] +'Reason: Received ICMP ERRPR Response type 3 and code 1, 2, 9, 10, or 13') # Open | Filtered
            elif resp.code == 3:
                print(p_str + status_str[2]+ 'Reason: Received ICMP ERROR Response type 3 and code 3') # Closed
        if resp.sport == int(pnum):
            print(p_str + status_str[0] + 'Reason : Received UDP reply.') # Open
    else:
        print(p_str + status_str[1] + 'Reason: No Response.') # Open | Filtered

def tcp_stealth_resp(resp, dest_ip, d_port, s_port):  # Helper function for parsing the TCP response
    status_str = ['Status: Open         ', 'Status: Open|Filtered       ', 'Status: Closed       ', 'Status: Filtered       ']
    p_str = 'Port: [' + str(d_port) + ']        '
    if resp == None:
        print(p_str + status_str[1] + 'Reason: No Response.') # Open | Filtered
    elif(resp.haslayer(TCP)):
        if(resp.getlayer(TCP).flags == 0x12):
            sr(IP(dst=dest_ip)/TCP(sport=s_port,dport=d_port,flags='R'), timeout = 10)
            print(p_str + status_str[0] + 'Reason: Received TCP reply.')
        elif resp.getlayer(TCP).flags == 0x14:
            print(p_str + status_str[2] + 'Reason: port closed.')
        elif resp.haslayer(ICMP):
            if int(resp.getlayer(ICMP).type) == 3 and int(resp.getlayer(ICMP).code) in [1,2,3,9,10,13]:
                print(p_str + status_str[3] + 'Reason: Received ICMP ERRPR Response type 3 and code 1, 2, 9, 10, or 13')

is_udp = False

# regex for parsing IP input
ip_regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"

# input parsing --------------
port_regex = ""
if len(sys.argv) < 3:
    print("Missing Arguments...Try again.")
    quit()

host_IP = sys.argv[2]
port_nums = sys.argv[3]

port_nums = port_nums.split('-')

src_port = RandShort()

if sys.argv[1] == 'U':
    is_udp = True

if (re.search(ip_regex,host_IP)):
    print()
else:
    print("-> " + host_IP + " is not a valid IP address\nExiting.")
    quit()

if is_udp:
    print('Protocol UDP')
    print('UDP Scanning starts . . .')
else:
    print('Protocol TCP')
    print('TCP Scanning starts . . .')
print('Target ' + str(host_IP))
print('Ports ' + sys.argv[3])

# scapy util begins here -----------------

if len(port_nums) > 1:
    for p in range(int(port_nums[0]), int(port_nums[1])+1):
        src_port = RandShort()
        if not is_udp: # ----------- TCP
            pkt = IP(dst=host_IP)/TCP(dport = p, sport = src_port, flags='S')
            r = sr1(pkt, verbose = 0, timeout = 4)
            tcp_stealth_resp(r,host_IP, p, src_port)
        else:    # ---------- UDP
            pkt = IP(dst=host_IP)/UDP(dport = p, sport = src_port)
            r = sr1(pkt, verbose = 0, timeout = 4)
            udp_response(r, p)
else:
    if not is_udp: # -------- TCP
        pkt = IP(dst=host_IP)/TCP(dport = int(port_nums[0]), sport = src_port)
        r = sr1(pkt, verbose = 0, timeout = 4)
        tcp_stealth_resp(r,host_IP, int(port_nums[0]), src_port)
    else:     # ---------- UDP
        pkt = IP(dst=host_IP)/UDP(dport = int(port_nums[0]), sport = src_port)
        r = sr1(pkt, verbose = 0, timeout = 4)
        udp_response(r,int(port_nums[0]))



