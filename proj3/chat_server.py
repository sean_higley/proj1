# Project 3 & 4: Chat Server
# Sean Higley & Nathan Chanachai
# November 17, 2021
from socket import *
from threading import Thread
import os
import user
import time

user.mn() #initialize available users on server start
usr_signed_in = [] #tuple of user info and attatched socket
bufsize = 2048
portnum = 12000
rhost = "localhost"
s_serv = socket(AF_INET, SOCK_STREAM)

s_serv.bind((rhost,portnum))

s_serv.listen(35) # listens for up to 35 users at a time
sent = 'sent: '
recd = 'rec: '
def cli_thrd(connec):
    global usr_signed_in
    my_usrname = ""
    valid_acc = False
    auth_string = connec.recv(bufsize)
    auth_string = auth_string.decode('utf-8').replace('\n','').split(':')
    print(recd+str(auth_string))

    if auth_string[0] == 'HELLO':
        mg = 'HELLO'
        connec.send(bytes(mg,'ascii'))
        print(sent + mg)

    while not valid_acc:
        auth_string = connec.recv(bufsize)
        auth_string = auth_string.decode('utf-8').replace('\n','').split(':')
        print(recd+str(auth_string))     
        if auth_string[0] == 'AUTH': # --- AUTHENTICATION
            for usr in user.users:
                if usr.u_name == auth_string[1] and usr.u_pw == auth_string[2]:
                    valid_acc = True
                    print(usr.u_name + ' signed in')
                    my_usrname = usr.u_name
                    usr_signed_in.append((usr, connec))
                    mg = 'AUTHYES'
                    connec.send(bytes(mg,'ascii'))
                    print(sent+ mg)
                    time.sleep(1)
                    broadcast('SIGNIN:'+my_usrname)
                    
            if not valid_acc:
                connec.send(bytes('AUTHNO','ascii'))
                print(sent+"AUTHNO")
    while True:
        try:
            print('listening.......')
            cli_stream = connec.recv(bufsize)
            print(cli_stream)
            cli_stream = cli_stream.decode('utf-8').split(':')
            
            if cli_stream[0] == 'LIST': # --- LIST ACTIVE USERS
                user_name_list = []
                for i in usr_signed_in:
                    user_name_list.append(i[0].u_name)
                print(user_name_list)
                user_name_list = ", ".join(user_name_list)
                print(user_name_list)
                connec.send(bytes(user_name_list,'ascii'))
            
            elif cli_stream[0] == 'TO': # --- TO
                for usr in usr_signed_in:
                    if usr[0].u_name == cli_stream[1]:
                        msg_to_snd = 'FROM:'+my_usrname+":"+cli_stream[2]
                        usr[1].send(bytes(msg_to_snd, 'ascii'))
                
            elif cli_stream[0] == 'BYE':
                broadcast("SIGNOFF:"+my_usrname)
                for client in usr_signed_in:
                    if client[0].u_name == my_usrname:
                        usr_signed_in.remove(client)
                        connec.close()
        except:
            break
        


def broadcast(message):
    for client in usr_signed_in:
        client[1].send(bytes(message, 'ascii'))

while True:

    connection, address = s_serv.accept()

    print(address[0] + " connected ")
    t = Thread(target=cli_thrd, args=(connection,))
    t.start()
connection.close()
s_serv.close()



