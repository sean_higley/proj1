# Project 3: Chat Client
# Sean Higley & Nathan Chanachai
# November 5, 2021
from socket import *
from threading import Thread
import os

# INSTRUCTIONS
#   run the commands:
#       >> python3 chat_client.py
# you will be prompted to enter a username and password
# the server address and port number are hard coded for you, though,
# if their values are set to None in lines 19 and 20 then the
# user will be prompted to enter them at runtime. 

#---------------------------------------------------------------

bufsize = 512
portnum = None                 # normally set to None
host_addr = None     # normally set to None
user_option = None

user_name = None    # 'test1'
passwrd = None      # 'p000'

s = socket(AF_INET,SOCK_STREAM) # create socket

# format terminal
os.system('clear')
print("Welcome to the Chat App")

#username,pw,port,server
while host_addr is None:
    host_addr = input('Enter the server address: ')
while portnum is None:
    portnum = int(input('Enter the port number: '))

#create connection
s.connect_ex((host_addr,portnum))

def authenticate_usr(): # function to authenticate the user
    global s, user_name, passwrd
    user_name = None
    passwrd = None
    while user_name is None:
        user_name = input('Enter a username: ')
    while passwrd is None:
        passwrd = input('Enter a password: ')
    auth_string = bytes('AUTH:'+user_name+':'+passwrd, 'ascii')
    s.send(auth_string)
    auth_string = s.recv(bufsize)
    auth_string = auth_string.decode('utf-8').replace('\n','')
    if auth_string == 'AUTHNO': 
        print('failed to authenticate username or password.')
        authenticate_usr()
    if auth_string == 'AUTHYES': 
        print('You are now authenticated.')

# create a thread function to recieve messages while prompt is active
def input_stream():
    global s # socket opened in beginngin of main
    while True:
        try:
            server_stream = s.recv(bufsize)                 # recieve payload
            server_stream = server_stream.decode('utf-8')   # decode payload
            server_stream = server_stream.split(':')        # format for parsing
            if server_stream[0] == 'FROM':                  # print message from user
                print('MESSAGE from '+ server_stream[1]+ ': ' + server_stream[2].replace('\n',''))
            elif server_stream[0] == 'SIGNOFF':             # print sign-off notif
                print('--User '+ server_stream[1].replace('\n','') + ' signed off.')
            elif server_stream[0] == 'SIGNIN':              # print sign-in notif
                print('--User '+server_stream[1].replace('\n','') + ' signed in')
            else:
                server_stream = server_stream[0].split(', ')# split list of signed-in
                print('~'+str(len(server_stream))+'~ Users currently logged in:')
                for usr in server_stream:
                    nm = usr.replace('\n','')
                    if nm == user_name:
                        print('     '+nm+'(You)')
                    else:
                        print('     '+nm)
            print('\nChoose an option: \n [1] List online users\n [2] Send someone a message\n [3] Sign off')
        except:
            break

s.send(bytes('HELLO','ascii')) # establish connection to server
authmsg = s.recv(bufsize)
authmsg = authmsg.decode('utf-8').replace('\n','')
if authmsg == 'HELLO':
    authenticate_usr()
else:
    print('failed to authenticate connection to server.')
    s.close()
    exit()


t = Thread(target=input_stream) # initialize thread
t.start()                       # start thread

# prompting for client 
while user_option != 3:
    user_option = int(input())
    if user_option == 1:                # option to list current users
        s.send(bytes('LIST','ascii'))
    if user_option == 2:                # option to send message
        usr2snd = input('User you would like to message: ')
        msg2snd = input('Message: ')
        s.send(bytes('TO:'+usr2snd+':'+msg2snd, 'ascii')) # send message through socket

print('Signing you off...')
s.send(bytes('BYE','ascii'))
s.close()



