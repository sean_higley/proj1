# Proj 2 Traceroute
# Sean Higley & Nathan Chanachai (Fall 2021)


# 177_proj2
# Project 2, Python using Scapy. Sean H. and Nathan C.

# IMPORTANT: run with sudo

# From within the proj2 folder run:

# 	>> sudo python3 traceroute.py <IP ADDRESS HERE>

# If you do not specify an IP address and run
# 	>> sudo python3 traceroute.py
# you will be prompted to enter an IP address. 


from scapy.all import IP, UDP, sr1
import re
import random
import sys

#regex to validate IPv4 address
ip_regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"

#get user input
if len(sys.argv) < 2:
    host_IP = input("Enter an  destination IP\n    ex: 4.2.2.4\n >>>  ")
else:
    host_IP = sys.argv[1]

if (re.search(ip_regex,host_IP)):
    print()
else:
    print("-> " + host_IP + " is not a valid IP address\nExiting.")
    quit()

# construct packets in loop
for t_t_l in range(1, 21): # ranges over ttl [1-20]
   
   #create UDP packet encapsulated by IPv4 packet
    p = IP(dst = host_IP, ttl = t_t_l) / UDP(dport = random.randrange(33434,33464)) # create packet

    #send
    r = sr1(p, verbose=0, timeout=10)
    if r is None:
        break
    #if port unreachable stop.
    elif r.type == 3:
        print(" IP: " + str(r.src) +(" "*(20 - len(r.src)))+" ttl: " + str(t_t_l) + "    on dport " + str(p[UDP].dport) + " ---> Port Unreachable")
        break
    elif r.type == 0:
        print('Net is Unreachable ...')
    elif r.type == 1:
        print("Host is Unreachable ...")
    else:
        print(" IP: " + str(r.src) +(" "*(20 - len(r.src)))+" ttl: " + str(t_t_l) + "    on dport " + str(p[UDP].dport))
